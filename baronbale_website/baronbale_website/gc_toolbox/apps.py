from django.apps import AppConfig


class GcToolboxConfig(AppConfig):
    name = 'gc_toolbox'
